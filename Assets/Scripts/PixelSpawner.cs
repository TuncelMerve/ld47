﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelSpawner : MonoBehaviour
{
	public GameObject pixelObj;
	public GameObject mainCamera;
	public float gap = 0.5f;
	public float spawnTimeGap = 0.5f;
	public float spawnArea = 6.0f;
	public float movementUpDown = .1f;
	private float nextSpawnTime = 0.0f;
	public List<GameObject> pixelList;
	private List<GameObject> destroyPixelList;
	private float ySpawnPosition = 0.0f;
	private bool isSpawning = false;
	//private int recordingIndex
	private List<float> recordPixels;
	private int playIndex = 0;

	private float mPrevX;
	private Color mPrevColor;
	private Color mNextColor;

	private Vector3 wrld;

	// Start is called before the first frame update
	void Start()
    {
		//set a random color for first pixel
	    mPrevColor = new Color
	    {
		    r = Random.Range(50, 255) / 255f,
		    g = Random.Range(50, 255) / 255f,
		    b = Random.Range(50, 255) / 255f,
		    a = 1
	    };

	    pixelList = new List<GameObject>();
		destroyPixelList = new List<GameObject>();

	    recordPixels = new List<float>();

		wrld = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, 0.0f));
		float half_sz = 50.0f;
	}

    // Update is called once per frame
    void Update()
    {		
		if (isSpawning && Time.time > nextSpawnTime)
		{

			nextSpawnTime = Time.time + spawnTimeGap;

			//update position
			
			UpdateSpawnPosition();
			SpawnPixel(false);
		}
		
		CheckDestroy();
	}

	void SpawnPixel(bool isInitial)
	{
		//Debug.Log(mainCamera.transform.position.x);
		GameObject newObj = Instantiate(pixelObj, new Vector3(mPrevX + gap, this.ySpawnPosition, 0), Quaternion.identity);
		mPrevX += gap;

		//set color
		var newColor = mPrevColor;
		float H, S, V;
		Color.RGBToHSV(newColor, out H, out S, out V);
		H += Time.deltaTime / 4;

		newColor = Color.HSVToRGB(H, S, V);

		newObj.GetComponent<SpriteRenderer>().color = newColor;
		mPrevColor = newColor;

		//Debug.Log(newObj.ToString());

		pixelList.Add(newObj);

		if (RoundManager.Instance.currentRound == RoundManager.RoundType.Recording && !isInitial)
			recordPixels.Add(this.ySpawnPosition);
	}

	void CheckDestroy()
	{
		destroyPixelList.Clear();

		foreach (GameObject pixelObj in pixelList)
		{
			//Debug.Log(pixelObj.ToString());
			if(pixelObj.transform.position.x < mainCamera.transform.position.x - spawnArea)
			{
				destroyPixelList.Add(pixelObj);				
			}
			else
			{
				if(destroyPixelList.Count > 0)
				{
					foreach (GameObject destroyPixelObj in destroyPixelList)
					{
						pixelList.Remove(destroyPixelObj);
						Destroy(destroyPixelObj);
					}						
				}
				return;
			}
		}
	}


	void UpdateSpawnPosition()
	{
		if (RoundManager.Instance.currentRound == RoundManager.RoundType.Playing)
		{
			if(playIndex < recordPixels.Count)
			{
				ySpawnPosition = recordPixels[playIndex];
				playIndex++;
			}			
		}
		else
		{
			if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
			{
				if((ySpawnPosition + movementUpDown) < (wrld.y+2))
					ySpawnPosition += movementUpDown;
			}
			else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
			{
				if ((ySpawnPosition - movementUpDown) > (wrld.y - 2))
					ySpawnPosition -= movementUpDown;
			}
		}
			
	}
	public void StartSpawning()
	{
		pixelList = new List<GameObject>();
		destroyPixelList = new List<GameObject>();

		if (RoundManager.Instance.currentRound == RoundManager.RoundType.Recording)
		{
			recordPixels = new List<float>();
		}

		mPrevX = mainCamera.transform.position.x - 4;
		ySpawnPosition = 0;
		playIndex = 0;

		for (var i = 0; i < 55; i++)
		{
			SpawnPixel(true);
		}

		isSpawning = true;

	}
	public void StopSpawning()
	{
		isSpawning = false;
	}

	public void ClearPixels()
	{
		foreach (var pix in pixelList)
		{
			Destroy(pix);
		}
		pixelList.Clear();
	}
}

