﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
	public static MainCamera Instance { get; private set; }
	private bool isMoving = false;



    // Update is called once per frame
    void Update()
    {
		if (isMoving)
		{
			Vector3 bob = Vector3.left * -Time.deltaTime / 2 * RoundManager.Instance.WireSpeed;
			this.transform.Translate(bob);
		}
			
    }
	private void Awake()
	{
		if (Instance == null) { Instance = this; } else if (Instance != this) { Destroy(this); }
		//DontDestroyOnLoad(gameObject);
	}
	public void StartMoving()
	{
		isMoving = true;
	}
	public void StopMoving()
	{
		isMoving = false;
	}
}
