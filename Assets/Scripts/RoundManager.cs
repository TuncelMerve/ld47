﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
	public enum RoundType
	{
		Recording,
		Playing
	}

	[SerializeField] private GameObject _mEndGameScreen;
	[SerializeField] private TMP_Text _mGameOverText;
	[SerializeField] private GameObject _mPlayer1Recording;
	[SerializeField] private GameObject _mPlayer2Recording;
	[SerializeField] private ScoreManager _mScoreManager;

	public static RoundManager Instance { get; private set; }

	public float timePerRound = 30.0f;
	public PixelSpawner pixelSpawner;
	public HaloMovement halo;
	public MainCamera cameraObj;

	private float roundStart = 0.0f;

	private bool isInRound = false;
	public RoundType currentRound;

	public bool IsGameOver;
	public bool ShouldRunRecorded;
	public int CurrentPlayer = 0;
	public float WireSpeed = 1;
	private bool isFail = false;

	private void Awake()
	{
		if (Instance == null) { Instance = this; } else if (Instance != this) { Destroy(this); }
		//DontDestroyOnLoad(gameObject);
	}
	
	// Start is called before the first frame update
	void Start()
    {
		//StartRound();
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.R))
		{
			currentRound = RoundType.Recording;
			StartRound();
		}
		else if (Input.GetKeyDown(KeyCode.P))
		{
			currentRound = RoundType.Playing;
			StartRound();
		}

	}

	public void GameOver()
	{
		IsGameOver = true;
		isFail = true;
		pixelSpawner.StopSpawning();
		StopRound();
	}	   

	public void StartRound()
	{
		isFail = false;
		isInRound = true;
		cameraObj.StartMoving();
		pixelSpawner.StartSpawning();
		Invoke("DelayAfterRound", timePerRound);
		StartCoroutine(SetLockOnHoop(false, 1f));
		halo.transform.localPosition = new Vector3(-3, 0, 10);		
	}
	void DelayAfterRound()
	{
		//cameraObj.StopMoving();
		pixelSpawner.StopSpawning();
		//halo.IsLocked = true;
		Invoke("StopRound", 11);
	}
	void StopRound()
	{
		CancelInvoke();	

		_mEndGameScreen.SetActive(true);
	
		var prevPlayer = CurrentPlayer;
		
		if(currentRound == RoundType.Playing)
		{
			var nextPlayer = CurrentPlayer;
			if (isFail)
			{
				_mGameOverText.text = string.Format("Player {0} failed.\nGet ready Player {1}", prevPlayer + 1, nextPlayer + 1);
			}
			else
			{
				_mGameOverText.text = string.Format("Player {0} succeeded.\nGet ready Player {1} to record a new level. Player {0} get ready to play.", prevPlayer + 1, (CurrentPlayer + 1) % 2 + 1);
				_mScoreManager.AddPlayerScore(prevPlayer + 1, 1);
			}	
		}
		else //recording round
		{
			var nextPlayer = (CurrentPlayer + 1) % 2;

			if (isFail)
			{
				_mGameOverText.text = string.Format("Player {0} failed.\nGet ready Player {1}", prevPlayer + 1, nextPlayer + 1);
			}
			else
			{
				_mGameOverText.text = string.Format("Player {0} succeeded.\nGet ready Player {1} to play your created level. Player {0} standby.", prevPlayer + 1, nextPlayer + 1);

				_mScoreManager.AddPlayerScore(prevPlayer + 1, 1);
			}
		}

		

		halo.IsLocked = true;
		halo.transform.localPosition = new Vector3(-3, 0, 10);
	}

	IEnumerator SetLockOnHoop(bool isLocked, float delayTime = 0)
	{
		yield return new WaitForSeconds(delayTime);
		halo.IsLocked = isLocked;
	}


	public void LoadGame()
	{		
		_mEndGameScreen.SetActive(false);
		IsGameOver = false;
		pixelSpawner.ClearPixels();

		SetLockOnHoop(true);
		halo.transform.localPosition = new Vector3(-3, 0, 10);

		if (ShouldRunRecorded)
		{
			ReplayRound();
		}
		else
		{
			NewRound();
		}

		ShouldRunRecorded = !ShouldRunRecorded;
	}

	public void ReplayRound()
	{		
		CurrentPlayer++;
		CurrentPlayer = CurrentPlayer % 2;
		updateRecordButtons(false);
		currentRound = RoundType.Playing;
		StartRound();

	}

	public void NewRound()
	{
		currentRound = RoundType.Recording;
		updateRecordButtons(true);
		StartRound();
	}

	private void updateRecordButtons(bool isRecording)
	{
		if (isRecording)
		{
			if (CurrentPlayer == 0)
			{
				_mPlayer1Recording.SetActive(false);
				_mPlayer2Recording.SetActive(true);
			}
			else
			{
				_mPlayer1Recording.SetActive(true);
				_mPlayer2Recording.SetActive(false);
			}
		}
		else
		{
			_mPlayer1Recording.SetActive(false);
			_mPlayer2Recording.SetActive(false);
		}
		
	}
}
