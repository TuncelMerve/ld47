﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaloMovement : MonoBehaviour
{
	public float speed = 20f;
	public Transform spriteObject;
	public bool IsLocked;

	private Plane dragPlane;
	private Vector3 offset;

	private Camera myMainCamera;
	private Vector3 world;

	public PixelSpawner pixelSpawner;

	void Start()
	{
		IsLocked = true;
		myMainCamera = Camera.main;
		dragPlane = new Plane(myMainCamera.transform.forward, transform.position);
	}

	void Update()
	{
		if(IsLocked) return;

		spriteObject.Rotate(Vector3.back, speed * Time.deltaTime);

		var camRay = myMainCamera.ScreenPointToRay(Input.mousePosition);

		float planeDist;
		dragPlane.Raycast(camRay, out planeDist);
		var pos = camRay.GetPoint(planeDist);
		//pos.x = myMainCamera.transform.position.x;

		//clamp
		world = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f));
		pos.x = world.x - 3;

		var curPixel = 0f;

		if (pixelSpawner.pixelList.Count > 11)
		{
			curPixel = (pixelSpawner.pixelList[9].transform.position.y + pixelSpawner.pixelList[11].transform.position.y) / 2f;	
		}
		pos.y = Mathf.Clamp(pos.y, curPixel - 0.3f, curPixel + 0.3f);

		transform.position = pos;
	}
}
