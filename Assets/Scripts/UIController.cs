﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	[SerializeField] private Button _mStartGame = null;
	[SerializeField] private Button _mCredits = null;
	[SerializeField] private Button _mCreditsBack = null;

	[SerializeField] private Button _mNewGame = null;

	[SerializeField] private GameObject _mAboutPanel;
	[SerializeField] private GameObject _mTitlePanel;

	private void Start()
	{
		Debug.Assert(_mStartGame != null);
		Debug.Assert(_mCredits != null);
		Debug.Assert(_mNewGame != null);

		_mStartGame.onClick.AddListener(StartGame);
		_mCredits.onClick.AddListener(ShowCredits);
		_mCreditsBack.onClick.AddListener(Back);

		_mNewGame.onClick.AddListener(RoundManager.Instance.LoadGame);
	}

	void StartGame()
	{
		_mTitlePanel.SetActive(false);
		RoundManager.Instance.StartRound();
	}

	private void ShowCredits()
	{
		_mAboutPanel.SetActive(true);

		var graphicsComponents = _mAboutPanel.GetComponentsInChildren<Graphic>();

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeIn(component, 0.5f);
		}

		GraphicExtensions.FadeIn(_mAboutPanel.GetComponent<Graphic>(), 0.5f);
	}

	private void Back()
	{
		var graphicsComponents = _mAboutPanel.GetComponentsInChildren<Graphic>();

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeOut(component);
		}

		GraphicExtensions.FadeOut(_mAboutPanel.GetComponent<Graphic>());

		Invoke("HideAboutPanel", 0.3f);
	}

	private void HideAboutPanel()
	{
		_mAboutPanel.SetActive(false);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene("MainScene");
		}
	}

}


public static class GraphicExtensions
{
	public static void FadeIn(Graphic g, float time = 2f)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(0f);
		g.CrossFadeAlpha(1f, time, false);
	}

	public static void FadeOut(Graphic g)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(1f);
		g.CrossFadeAlpha(0f, .3f, false);
	}
}