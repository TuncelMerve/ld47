﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
	[SerializeField] private TMP_Text _mPlayer1Text;
	[SerializeField] private TMP_Text _mPlayer2Text;

	private int player1Score = 0;
	private int player2Score = 0;

	private bool player1Completed = false;
	private bool player2Completed = false;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void AddPlayerScore(int playerNum, int scoreToAdd)
	{
		if (playerNum == 1)
		{
			player1Score += scoreToAdd;
			_mPlayer1Text.text = "Player 1: " + player1Score;
		}
		else
		{
			player2Score += scoreToAdd;
			_mPlayer2Text.text = "Player 2: " + player2Score;
		}		
	}
	public void ResetScores()
	{
		player1Score = 0;
		player2Score = 0;
		_mPlayer1Text.text = "Player 1: 0";
		_mPlayer1Text.text = "Player 2: 0";
	}
	//public void setPlayerCompleted(int playerNum)
	//{
	//	if(playerNum == 1)
	//	{
	//		player1Completed = true;
	//	}
	//	else
	//	{
	//		player2Completed = true;
	//	}
	//}

	//public void resetPlayerCompleted()
	//{
	//	player1Completed = false;
	//	player2Completed = false;
	//}
}
